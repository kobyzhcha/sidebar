import { ReactComponent as CupIcon } from './cup.svg';
import { ReactComponent as BricksIcon } from './bricks.svg';
import { ReactComponent as UserIcon } from './user.svg';
import { ReactComponent as HandsIcon } from './hands.svg';
import { ReactComponent as DiagramIcon } from './diagram.svg';

export const links = [
  {
    name: 'Event details',
    icon: CupIcon,
  },
  {
    name: 'Sessions',
    icon: BricksIcon,
  },
  {
    name: 'Participants',
    icon: UserIcon,
  },
  {
    name: 'Companies',
    icon: HandsIcon,
    subLinks: [{ name: 'Contacts' }],
  },
  {
    name: 'Analytics',
    icon: DiagramIcon,
  },
];
