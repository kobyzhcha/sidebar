import React from 'react';
import styles from './index.module.scss';
import cls from 'classnames';
import { ReactComponent as ArrowIcon } from '../arrowDown.svg';

const Link = ({ link }) => {
  const [opened, setOpened] = React.useState(false);

  return (
    <div className={styles.wrapper}>
      <div
        className={cls(styles.link, { [styles.opened]: opened })}
        onClick={() => (link.subLinks ? setOpened(prev => !prev) : {})}
      >
        <div className={styles.info}>
          <link.icon />
          {link.name}
        </div>
        {!!link.subLinks && <ArrowIcon className={cls(styles.arrow, { [styles.arrowDown]: !opened })} />}
      </div>
      {opened &&
        link.subLinks &&
        link.subLinks.map(sub => (
          <div key={sub.name} className={styles.subLink}>
            {sub.name}
          </div>
        ))}
    </div>
  );
};

export default Link;
