import React from 'react';
import Link from '../Link';
import cls from 'classnames';
import styles from './index.module.scss';
import { links } from '../links';
import { ReactComponent as ArrowLeftIcon } from '../arrowLeft.svg';

const Sidebar = () => {
  const [minimized, toggleMinimized] = React.useState(false);

  return (
    <div className={cls(styles.wrapper, { [styles.minimized]: minimized })}>
      <div className={styles.top}>
        <div className={styles.minimizeButton} onClick={() => toggleMinimized(prev => !prev)}>
          <ArrowLeftIcon />
          <span>Dashboard</span>
        </div>
        <div className={styles.title}>Event Talent Summit 2020</div>
      </div>
      <div className={styles.links}>
        {links.map(link => (
          <Link key={link.name} link={link} />
        ))}
      </div>
    </div>
  );
};

export default Sidebar;
